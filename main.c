#include "ext2_sim.h"

int main(int argc, char* argv[])
{
	char* device = "fs";
	
	if (argc > 1)
		device = argv[1];
	
	ext2_sim_run(device);
	
	return 0;
}
