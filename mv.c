#include "ext2_sim.h"

int ext2_mv(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("Must src, target\n");
		return 0;
	}
	
	char* new_argv[] = {"link", argv[1], argv[2]};
	int result = ext2_link(3, new_argv);
	
	if (!result)
	{
		printf("Failed to link file...\n");
		return 0;
	}
	else
	{
		DEBUG("File linked ok... now to delete old\n");
	}
	
	result = ext2_unlink(2, new_argv);
	
	if (!result)
	{
		printf("Failed to delete original\n");
		return 0;
	}
	else
	{
		DEBUG("Deleted original OK!\n");
	}
	
	return 1;
}
