#include "ext2_sim.h"

int ext2_open_actual(char* path, int mode)
{
	INODE inode;
	int inum;
	int result = path_to_inode(path, &inode, &inum, FOLLOW_SYMLINKS);
	
	if (!result)
	{
		DEBUG("File `%s' could not be found\n", path);
		
		if (mode != MODE_R)
		{
			char* argv_new[] = {"creat", path};
			int r = ext2_creat(2, argv_new);
			
			if (!r)
			{
				printf("Failed to creat file\n");
				return -1;
			}
			else
			{
				DEBUG("Made the new file\n");
				result = path_to_inode(path, &inode, &inum, FOLLOW_SYMLINKS);
			}
			
		}
		else
		{
			printf("File will not be created when opening for read\n");
			return -1;
		}
	}
	else
	{
		DEBUG("File found\n");
	}
	
	int file_mode = get_file_mode(inum);
	
	if (file_mode == MODE_W)
	{
		printf("File is already open in w/rw/a mode\n");
		return -1;
	}
	else if (file_mode == MODE_R)
	{
		if (mode != MODE_R)
		{
			printf("File is already open for read, you may not write to it\n");
			return -1;
		}
	}
	
	DEBUG("We are ok to open the file in this mode!\n");
	
	FD fd;
	fd.inum = inum;
	fd.mode = mode;
	strncpy(fd.file_name, path, 256);
	
	switch (mode)
	{
		case MODE_W:
			truncate_file(inode, inum);
			// fall-through on purpose
			
		case MODE_R:
		case MODE_RW:
			fd.offset = 0;
			break;
		
		case MODE_A:
			fd.offset = inode.i_size;
			break;
			
		default:
			printf("An unrecognized mode made it this far?\n");
			return -1;
	}
	
	result = put_in_fd_table(fd);
	
	if (result < 0)
	{
		printf("No more available file descriptors\n");
		return -1;
	}
	
	DEBUG("Opened file `%s' for %s with fd = %d\n", path, file_open_mode_to_str(mode), result);
	
	return result;
}

// returns -1 on error, not 0 like most of these functions...
int ext2_open(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must specify a file and optionally a mode\n");
		return -1;
	} 
	else if (argc < 3)
	{	
		DEBUG("Mode defaulted to READ\n");
		return ext2_open_actual(argv[1], MODE_R);
	}
	else
	{
		return ext2_open_actual(argv[1], str_to_file_open_mode(argv[2]));
	}	
}
