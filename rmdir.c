#include "ext2_sim.h"

int ext2_rmdir(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must supply a directory to remove\n");	
		return 0;
	}
	
	INODE inode;
	int inum;
	int result;
	
	result = path_to_inode(argv[1], &inode, &inum, FOLLOW_SYMLINKS_EXCEPT_LAST);
	
	if (!result)
	{
		printf("Invalid path `%s'\n", argv[1]);
		return 0;
	}
	
	if ((inode.i_mode & S_IFLNK) == S_IFLNK)
	{
		DEBUG("Use rm for deleting symlinks\n");
		return 0;
	}
	else if (inode.i_mode & S_IFDIR)
	{
		DEBUG("Ok, this is a directory...\n");
	}
	else
	{
		DEBUG("Rmdir cannot delete a file, use rm instead\n");
		return 0;
	}
	
	
	if (!directory_is_empty(inode))
	{
		printf("Directory is not empty\n");
		return 0;
	}
	
	if (directory_is_busy(inum))
	{
		printf("Directory is busy\n");
		return 0;
	}

	DEBUG("Truncating file...\n");
	truncate_file(inode, inum);
	inode_dealloc(inum);
	
	char* path = argv[1];
	char dcopy1[BSIZE];
	char dcopy2[BSIZE];
	
	strcpy(dcopy1, path);
	strcpy(dcopy2, path);
	
	DEBUG("path=%s\n", path);
	
	char* parent = dirname(dcopy1);
	char* child = basename(dcopy2);
	
	INODE parent_inode;
	int parent_inum;
	
	result = path_to_inode(parent, &parent_inode, &parent_inum, FOLLOW_SYMLINKS);
	
	parent_inode.i_links_count--;
	write_inode(parent_inode, parent_inum);
	
	if (!result)
	{
		printf("Serious problem, parent cannot be found?\n");
		return 0;
	}
		
	result = remove_child(parent_inode, child);
	
	if (!result)
	{
		printf("Child not found to remove\n");
		return 0;
	}
		
	return 1;
}
