#include "ext2_sim.h"

int ext2_chmod(int argc, char* argv[])
{
	int result;
	INODE inode;
	int inum;
	
	if (argc < 3)
	{
		printf("Must specify a mode and a file\n");
		return 0;
	}
	
	result = path_to_inode(argv[2], &inode, &inum, FOLLOW_SYMLINKS);

	if (!result)
	{
		printf("File does not exist\n");
		return 0;
	}
	
	// parse the mode
	int old_mode = inode.i_mode;
	DEBUG("old_mode = %x\n", old_mode);
	
	int info_mask = old_mode & S_IFMT;
	DEBUG("inf_mask = %x\n", info_mask);
	
	int in_mode;
	sscanf(argv[1], "%o", &in_mode);
	DEBUG(" in_mode = %x\n", in_mode);
	
	if (in_mode > 0777)
	{
		printf("Invalid mode, must be less than 0777\n");
		return 0;
	}
	
	int new_mode = info_mask | in_mode;
	DEBUG("new_mode = %x\n", new_mode);

	inode.i_mode = new_mode;
	write_inode(inode, inum);
	
	return 1;	
}
