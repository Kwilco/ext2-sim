#include "ext2_sim.h"

int ext2_read_actual(int fd, char* buf, int nbytes)
{
	if (fd < 0 || fd > BSIZE)
	{
		printf("fd out of range\n");
		return -1;
	}
	
	FD* fd_table = get_fd_table();
	
	if (fd_table[fd].inum == 0)
	{
		printf("This fd is not open\n");
		return -1;
	}
	
	if (fd_table[fd].mode == MODE_A || fd_table[fd].mode == MODE_W)
	{
		printf("Cannot read this file, it is open for %s\n", file_open_mode_to_str(fd_table[fd].mode));
		return -1;
	}
	
	char* out = buf;
	int count = 0;
	
	INODE inode = inumber_to_inode(fd_table[fd].inum);
	
	int size = inode.i_size - fd_table[fd].offset;
	
	while (nbytes > 0 && size > 0)
	{
		int lblk = fd_table[fd].offset / BSIZE;
		int byte_offset = fd_table[fd].offset % BSIZE;
		int block = logical_block_to_block(inode, lblk);
		
		char readbuf[BSIZE];
		rblock(readbuf, block);
		
		char* cp = readbuf + byte_offset;
		int remain = BSIZE - byte_offset;
		
		while (remain > 0)
		{
			*out++ = *cp++;
			fd_table[fd].offset++;
			count++;
			size--;
			nbytes--;
			remain--;
			if (nbytes <= 0 || size <= 0)
				break;
		}
	}
	
	return count;
}


int ext2_read(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("Must specify file descriptor and amount to read\n");
		return -1;
	}
	
	int fd = atoi(argv[1]);
	int size = atoi(argv[2]);
	
	if (size < 0 || size > (10 * 1024 * 1024)) // 10 megs, just to prevent ridiculous requests right now
	{
		printf("Invalid size\n");
		return -1;
	}
	
	char* buf = (char*) malloc(size);
	
	int result = ext2_read_actual(fd, buf, size);
	
	if (result < 0)
	{
		printf("Error...\n");
		return result;
	}
	else
	{
		printf("ext2_read read %d bytes from fd %d, bytes:\n%s\n", result, fd, buf);
		return result;
	}
}
