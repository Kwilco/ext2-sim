#include "ext2_sim.h"

int ext2_lseek(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("Must supply fd and offset\n");
		return -1;
	}
	
	int fd = atoi(argv[1]);
	int offset = atoi(argv[2]);
	
	if (fd < 0 || fd > BSIZE)
	{
		printf("fd out of range\n");
		return -1;
	}
	
	FD* fd_table = get_fd_table();
	
	if (fd_table[fd].inum == 0)
	{
		printf("This fd is not open\n");
		return -1;
	}
	
	int original_offset = fd_table[fd].offset;
	INODE inode = inumber_to_inode(fd_table[fd].inum);
	
	if (offset < 0 || offset > (int) inode.i_size)
	{
		printf("Offset out of range, must be between 0 and %d\n", (int) inode.i_size);
		return -1;
	}
	
	fd_table[fd].offset = offset;
	
	printf("%s offset changed from %d to %d\n", fd_table[fd].file_name, original_offset, offset);

	return original_offset;
}
