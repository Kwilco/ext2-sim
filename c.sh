rm ./ext2_sim

rm fs
touch fs
sudo mkfs fs 1440 < mkfs_answers.txt

./m.sh

pushd .
cd /mnt

# do stuff here
sudo mkdir D
sudo mkdir D/AD
sudo touch D/AD/F

sudo touch F
sudo ln -s  F FLINK

sudo ln -s D/AD DIRLINK
sudo ln -s /DIRLINK/F  FLINK2 


sudo echo little file >F
sudo dd if=/dev/urandom of=1KF bs=1K count=1


sudo dd if=/dev/urandom of=SMALLRAND bs=1K count=30
sudo dd if=/dev/urandom of=HUGERAND bs=1K count=512
#sudo dd if=/dev/urandom of=HUGERAND bs=1K count=1024
sudo dd if=/dev/urandom of=NOINDBLK bs=1K count=12

sudo cp ~/ext2_sim/fs_functions.c /mnt/fs.txt

popd

./u.sh

cc *.c  -Wall -Wextra  -o ext2_sim && 
#./ext2_sim fs < sim_commands.txt &&
./ext2_sim fs
