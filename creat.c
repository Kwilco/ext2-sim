#include "ext2_sim.h"

int ext2_creat_actual(INODE parent_inode, int parent_inum, char* child)
{
	DEBUG("creat, with parent inum = %d, child =%s\n", parent_inum, child);
	
	read_super();
	read_group();
	
	if (G_DESC.bg_free_inodes_count < 1)
	{
		printf("No free inodes.\n");
		return 0;
	}
	
	if (G_DESC.bg_free_blocks_count < 2)
	{
		printf("Need at least 2 free data blocks\n");
		return 0;
	}
	
	int inum = inode_alloc();
	
	if (!inum)
	{
		printf("No inum was returned\n");
		return 0;
	}
	else
	{
		DEBUG("Time to make an inode at inum %d!\n", inum);
	}
	
	INODE i;
	
	i.i_mode = S_IFREG | S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	i.i_uid = 0;
	i.i_size = 0;
	i.i_atime = time(NULL);
	i.i_ctime = time(NULL);
	i.i_mtime = time(NULL);
	i.i_dtime = 0;
	i.i_gid = 0;
	i.i_links_count = 1;
	i.i_blocks = 0;
	i.i_flags = 0;
	i.i_dir_acl = 0; // ARRRGGGGGGGGGGHHHHHHHHHHH crazy linux
	
	int j;
	for (j = 0; j <= EXT2_DIND_BLOCK; j++)
		i.i_block[j] = 0;

	// write it
	write_inode(i, inum);
	
	int result = add_child(parent_inode, parent_inum, child, inum);
	
	if (!result)
	{
		printf("Failed to add child\n");
	}
	else
	{
		DEBUG("Added child inode=%d\n", inum);
	}

	return result;
}

int ext2_creat(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must supply a filename to create\n");
		return 0;
	}
	
	char* dir = argv[1];
	char dcopy1[BSIZE];
	char dcopy2[BSIZE];
	
	strcpy(dcopy1, dir);
	strcpy(dcopy2, dir);
	
	DEBUG("dir=%s\n", dir);
	
	char* parent = dirname(dcopy1);
	char* child = basename(dcopy2);
	
	DEBUG("parent=%s, child=%s\n", parent, child);
	
	INODE parent_inode;
	int parent_inum;
	int found = 0;
	
	found = path_to_inode(parent, &parent_inode, &parent_inum, FOLLOW_SYMLINKS);
	
	if (!found) 
	{
		printf("Parent directory %s does not exist\n", parent);
		return 0;
	}
	else
	{
		DEBUG("Parent directory %s exists! inum=%d\n", parent, parent_inum);
	}
	
	INODE child_inode;
	int child_inum;
	found = find_inode(parent_inode, parent_inum, child, &child_inode, &child_inum, FOLLOW_SYMLINKS);
	
	if (found)
	{
		printf("Child %s already exists in directory %s\n", child, parent);
		return 0;
	}
	else
	{
		DEBUG("Child doesn't already exist!\n");
	}
	
	DEBUG("About to call creat_actual with parent_inum %d and child name %s\n", parent_inum, child);
	
	return ext2_creat_actual(parent_inode, parent_inum, child);
}
