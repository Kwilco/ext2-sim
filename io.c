#include "ext2_sim.h"

// in theory, only these functions would have to be replaced
// in order for this to function as a real file system

void rblock(char* buf, int block)
{
	lseek(ROOT_FD, block * BSIZE, SEEK_SET);
	read(ROOT_FD, buf, BSIZE);
}

void wblock(char* buf, int block)
{
	lseek(ROOT_FD, block * BSIZE, SEEK_SET);
	write(ROOT_FD, buf, BSIZE);
}
