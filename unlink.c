#include "ext2_sim.h"

int ext2_unlink(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must supply a path to unlink\n");	
		return 0;
	}
	
	DEBUG("Unlinking: %s\n", argv[1]);
	
	INODE inode;
	int inum;
	int result;
	
	result = path_to_inode(argv[1], &inode, &inum, FOLLOW_SYMLINKS_EXCEPT_LAST);
	
	if (!result)
	{
		printf("Invalid path `%s'\n", argv[1]);
		return 0;
	}
	
	if ((inode.i_mode & S_IFLNK) == S_IFLNK)
	{
		DEBUG("We're deleting a symlink, it's ok\n");
	}
	else if (inode.i_mode & S_IFDIR)
	{
		printf("Cannot delete a directory, use rmdir instead\n");
		return 0;
	}
	else
	{
		DEBUG("Confirmed that this is a file!\n");
	}
	
	inode.i_links_count--;
	write_inode(inode, inum);
	
	char* path = argv[1];
	char dcopy1[BSIZE];
	char dcopy2[BSIZE];
	
	strcpy(dcopy1, path);
	strcpy(dcopy2, path);
	
	DEBUG("path=%s\n", path);
	
	char* parent = dirname(dcopy1);
	char* child = basename(dcopy2);
	
	INODE parent_inode;
	int parent_inum;
	
	result = path_to_inode(parent, &parent_inode, &parent_inum, FOLLOW_SYMLINKS);
	
	if (!result)
	{
		printf("Serious problem, parent cannot be found?\n");
		return 0;
	}
		
	result = remove_child(parent_inode, child);
	
	if (!result)
	{
		printf("Child not found to remove\n");
		return 0;
	}
	
	if (inode.i_links_count < 1 && !file_is_busy(inum))
	{
		DEBUG("Truncating file...\n");
		truncate_file(inode, inum);
		inode_dealloc(inum);
	}
	else
	{
		DEBUG("Not truncating file just yet, there may be other links or it may be open somewhere\n");
	}
	
	return 1;
}
