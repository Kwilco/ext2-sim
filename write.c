#include "ext2_sim.h"

int ext2_write_actual(int fd, char* buf, int nbytes)
{
	if (fd < 0 || fd > BSIZE)
	{
		printf("fd out of range\n");
		return -1;
	}
	
	FD* fd_table = get_fd_table();
	
	if (fd_table[fd].inum == 0)
	{
		printf("This fd is not open\n");
		return -1;
	}
	
	if (fd_table[fd].mode == MODE_R)
	{
		printf("Cannot write to this file, it is open for %s\n", file_open_mode_to_str(fd_table[fd].mode));
		return -1;
	}
	
	INODE inode = inumber_to_inode(fd_table[fd].inum);
	
	int wrote = 0;
	
	while (nbytes > 0)
	{
		int lblk = fd_table[fd].offset / BSIZE;
		int byte_offset = fd_table[fd].offset % BSIZE;
		int block = logical_block_to_block(inode, lblk);
		
		if (block == 0)
		{
			write_inode(inode, fd_table[fd].inum);
			block = allocate_logical_block(inode, fd_table[fd].inum, lblk);
			inode = inumber_to_inode(fd_table[fd].inum); // regenerate changed inode
			
			if (block == 0)
			{
				printf("Could not allocate a new block\n");
				return -1;
			}
		}
		
		char wbuf[BSIZE];
		rblock(wbuf, block);
		
		char* cp = wbuf + byte_offset;
		char* cq = buf;
		int remain = BSIZE - byte_offset;
		
		while (remain > 0)
		{
			//DEBUG("Writing %d to %p\n", *cq, cp);
			*cp++ = *cq++;
			wrote++;
			nbytes--;
			remain--;
			fd_table[fd].offset++;
			if ((unsigned int) fd_table[fd].offset > inode.i_size)
				inode.i_size++;
			if (nbytes <= 0)
				break;				
		}
		write_inode(inode, fd_table[fd].inum);
		wblock(wbuf, block);
	}
	
	write_inode(inode, fd_table[fd].inum);
	return wrote;
}


int ext2_write(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must specify file descriptor\n");
		return -1;
	}
	
	int fd = atoi(argv[1]);
	
	char buf[BSIZE] = {0};
	char temp[BSIZE] = {0};
	
	printf("Input text to write to file, end with a blank line:\n");
	while (strcmp(temp, "\n") != 0)
	{
		fgets(temp, BSIZE, stdin);
		strncat(buf, temp, BSIZE);
	}
	
	DEBUG("Data input:\n%s", buf);
	
	int result = ext2_write_actual(fd, buf, strnlen(buf, BSIZE));
	
	if (result < 0)
	{
		printf("Error...\n");
		return result;
	}
	else
	{
		printf("ext2_write wrote %d bytes to fd %d\n", result, fd);
		return result;
	}
}
