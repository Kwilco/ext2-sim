#include "ext2_sim.h"

int ext2_link_actual(INODE parent_inode, int parent_inum, char* target_name, int target_inum)
{
	INODE i = inumber_to_inode(target_inum);
	i.i_links_count += 1;
	write_inode(i, target_inum);
	
	return add_child(parent_inode, parent_inum, target_name, target_inum);
}

int ext2_link(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("Must supply a a src and a target\n");
		return 0;
	}
	
	char* dir = argv[1];
	char dcopy1[BSIZE];
	char dcopy2[BSIZE];
	
	strcpy(dcopy1, dir);
	strcpy(dcopy2, dir);
	
	DEBUG("src dir=%s\n", dir);
	
	char* src_parent = dirname(dcopy1);
	char* src_child = basename(dcopy2);
	
	DEBUG("src_parent=%s, src_child=%s\n", src_parent, src_child);
	
	INODE src_parent_inode;
	int src_parent_inum;
	int found = 0;
	
	found = path_to_inode(src_parent, &src_parent_inode, &src_parent_inum, FOLLOW_SYMLINKS);
	
	if (!found) 
	{
		printf("Source parent directory %s does not exist\n", src_parent);
		return 0;
	}
	else
	{
		DEBUG("Source parent directory %s exists! inum=%d\n", src_parent, src_parent_inum);
	}
	
	INODE src_child_inode;
	int src_child_inum;
	found = find_inode(src_parent_inode, src_parent_inum, src_child, &src_child_inode, &src_child_inum, FOLLOW_SYMLINKS_EXCEPT_LAST);
	
	if (found)
	{
		DEBUG("Child exists in directory %s, as it should be!\n", src_parent);
	}
	else
	{
		printf("Source doesn't already exist!\n");
		return 0;
	}
	
	if (src_child_inode.i_mode & S_IFREG)
	{
		DEBUG("The source is a regular file\n");
	}
	else
	{
		printf("Source isn't a regular file\n");
		return 0;
	}
	
	dir = argv[2];
	
	strcpy(dcopy1, dir);
	strcpy(dcopy2, dir);
	
	DEBUG("dest dir=%s\n", dir);
	
	char* dst_parent = dirname(dcopy1);
	char* dst_child = basename(dcopy2);
	
	DEBUG("dst_parent=%s, dst_child=%s\n", dst_parent, dst_child);
	
	INODE dst_parent_inode;
	int dst_parent_inum;
	
	found = path_to_inode(dst_parent, &dst_parent_inode, &dst_parent_inum, FOLLOW_SYMLINKS);
	
	if (!found) 
	{
		printf("Dest parent directory %s does not exist\n", dst_parent);
		return 0;
	}
	else
	{
		DEBUG("Dest parent directory %s exists! inum=%d\n", dst_parent, dst_parent_inum);
	}
	
	INODE dst_child_inode;
	int dst_child_inum;
	found = find_inode(dst_parent_inode, dst_parent_inum, dst_child, &dst_child_inode, &dst_child_inum, FOLLOW_SYMLINKS);
	
	if (found)
	{
		printf("Dest child %s already exists in directory %s\n", dst_child, dst_parent);
		return 0;
	}
	else
	{
		DEBUG("Dest child doesn't already exist!\n");
	}
	
	return ext2_link_actual(dst_parent_inode, dst_parent_inum, dst_child, src_child_inum);
}
