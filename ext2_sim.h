#ifndef _EXT2_SIM_H_
#define _EXT2_SIM_H_

#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/fs.h>
#include <linux/ext2_fs.h>
#include <sys/stat.h>
#include <libgen.h>
#include <time.h>
#include <stdint.h>
#include <inttypes.h>

#define DEBUGGING
#ifdef DEBUGGING
#define DEBUG(format, args...) printf("\033[32m"format"\033[0m", ##args);
#else
#define DEBUG(format, args...)
#endif

// block size of fs
#define BSIZE 1024

#define EXT2_MAGIC 0xEF53

#define NO_FOLLOW_SYMLINK 0
#define FOLLOW_SYMLINKS 1
#define FOLLOW_SYMLINKS_EXCEPT_LAST 2 

#define MODE_R 1
#define MODE_W 2
#define MODE_RW 3
#define MODE_A 4


// useful reference for some ext2 constants
// http://uranus.chrysocome.net/explore2fs/es2fs.htm

typedef struct ext2_super_block SUPER;
typedef struct ext2_group_desc GD;
typedef struct ext2_inode INODE;
typedef struct ext2_dir_entry_2 DIR;

typedef struct s_file_descriptor
{
	int inum;
	int mode;
	long offset;
	char file_name[256];
} FD;


//globals, all of these should live in ext2_sim.c

// filename of root device
extern char* ROOT_DEVICE;

// root device linux file descriptor
extern int ROOT_FD;

// important filesystem objects
extern SUPER S_BLOCK;
extern GD G_DESC;
extern int INODES_BEGIN_BLOCK;

// current working directory
extern int CWD_INUM;



int handle_command(char* cmd);

void rblock(char* buf, int block);
void wblock(char* buf, int block);
INODE inumber_to_inode(int inum);
INODE root_inode(void);

int find_inode(INODE curr, int curr_inum, char* filepath, INODE* dest, int* dest_inum, int symlink);
int path_to_inode(char* path, INODE* inode, int* inum, int symlink);
int search_dir_inum_to_path(INODE* inode_ptr, int inum, char* buf);
void ext2_pwd_actual(int inum, char* buf);
int write_inode(INODE i, int inum);
int inode_alloc(void);
int inode_dealloc(int inum);
int block_alloc(void);

int directory_is_empty(INODE inode);
int directory_is_busy(int inum);

int logical_block_to_block(INODE inode, int offset);
int allocate_logical_block(INODE i, int inum, int lblk);

int truncate_file(INODE inode, int inum);

int add_child(INODE parent_inode, int parent_inum, char* child_name, int child_inum);
int remove_child(INODE parent_inode, char* child);

int min_len(DIR* d);
int ideal_len(char* str);
int test_bit(char* buf, int i);
void read_group(void);
void read_super(void);

int get_file_mode(int inum);
int put_in_fd_table(FD fd);
char* file_open_mode_to_str(int mode);
int str_to_file_open_mode(char* mode_str);

FD* get_fd_table(void);
int file_is_busy(int inum);

void ext2_sim_run(char* device);

// level 1
int ext2_mount_root(char* device);
int ext2_menu(int argc, char* argv[]);
int ext2_mkdir(int argc, char* argv[]);
int ext2_creat(int argc, char* argv[]);
int ext2_ls(int argc, char* argv[]);
int ext2_cd(int argc, char* argv[]);
int ext2_pwd(int argc, char* argv[]);
int ext2_stat(int argc, char* argv[]);
int ext2_touch(int argc, char* argv[]);
int ext2_link(int argc, char* argv[]);
int ext2_unlink(int argc, char* argv[]);
int ext2_chmod(int argc, char* argv[]);
int ext2_symlink(int argc, char* argv[]);
int ext2_rmdir(int argc, char* argv[]);

// level 2
int ext2_open(int argc, char* argv[]);
int ext2_pfd(int argc, char* argv[]);
int ext2_close(int argc, char* argv[]);
int ext2_lseek(int argc, char* argv[]);
int ext2_read(int argc, char* argv[]);
int ext2_write(int argc, char* argv[]);
int ext2_cat(int argc, char* argv[]);
int ext2_mv(int argc, char* argv[]);
int ext2_cp(int argc, char* argv[]);

// unix style file functions
int ext2_close_actual(int num);
int ext2_write_actual(int fd, char* buf, int nbytes);
int ext2_read_actual(int fd, char* buf, int nbytes);
int ext2_open_actual(char* path, int mode);

#endif
