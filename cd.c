#include "ext2_sim.h"

int ext2_cd(int argc, char* argv[])
{
	if (argc < 2)
	{
		// change to root
		CWD_INUM = EXT2_ROOT_INO;
		return 1;
	}
	
	int result;
	INODE inode;
	int inum;
	
	result = path_to_inode(argv[1], &inode, &inum, FOLLOW_SYMLINKS);
	
	if (!result)
	{
		printf("Could not change to that directory\n");
		return 0;	
	}
	
	CWD_INUM = inum;
	
	return 1;	
}
