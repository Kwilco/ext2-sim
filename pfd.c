#include "ext2_sim.h"

int ext2_pfd(int argc, char* argv[])
{
	FD* fd_table = get_fd_table();
	
	printf("%-30s  %-4s  %-4s  %-10s  %-7s\n", "filename", "inum", "fd", "mode", "offset");
	printf("%-30s  %-4s  %-4s  %-10s  %-7s\n",
	 "------------------------------", "----", "----", "----------", "-------");
	
	int i;
	for (i = 0; i < BSIZE; i++)
	{
		FD fd = fd_table[i];
		if (fd.inum != 0)
			printf("%-30s  %-4d  %-4d  %-10s  %-7ld\n",
			fd.file_name, fd.inum, i, file_open_mode_to_str(fd.mode), fd.offset);
	}
	
	return 1;
}
