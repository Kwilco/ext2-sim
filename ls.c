#include "ext2_sim.h"

void mode_to_str(mode_t mode, char* buf)
{
	if (mode & S_IFDIR)
		strcat(buf, "d");
	else if ((mode & S_IFLNK) == S_IFLNK)
		strcat(buf, "l");
	else
		strcat(buf, "-");

	strcat(buf, (mode & S_IRUSR) ? "r" : "-");
	strcat(buf, (mode & S_IWUSR) ? "w" : "-");
	strcat(buf, (mode & S_IXUSR) ? "x" : "-");
	strcat(buf, (mode & S_IRGRP) ? "r" : "-");
	strcat(buf, (mode & S_IWGRP) ? "w" : "-");
	strcat(buf, (mode & S_IXGRP) ? "x" : "-");
	strcat(buf, (mode & S_IROTH) ? "r" : "-");
	strcat(buf, (mode & S_IWOTH) ? "w" : "-");
	strcat(buf, (mode & S_IXOTH) ? "x" : "-");
}

int ext2_ls(int argc, char* argv[])
{
	int result;
	INODE inode;
	int inum;
	
	if (argc < 2)
		result = path_to_inode(".", &inode, &inum, FOLLOW_SYMLINKS);
	else
		result = path_to_inode(argv[1], &inode, &inum, FOLLOW_SYMLINKS);
	
	if (!result)
	{
		printf("Path does not exist\n");
		return 0;
	}
	
	// makes sure we're looking at a directory here... (oops)
	if (inode.i_mode & S_IFDIR)
	{
		DEBUG("this is a directory...\n");
	}
	else
	{
		printf("ls doesn't work on anything other than a directory\n");
		return 0;
	}
	
	printf("inode  mode        links  owner  group  bytes    last_mod      filename\n");
	
	int i;
	int b;
	for (i = 0; (b = logical_block_to_block(inode, i)) != 0; i++)
	{
		char fbuf[BSIZE];
		rblock(fbuf, b);

		DIR* dir = (DIR*) fbuf;
		char* cp = (char*) dir;
		
		while ((char*) dir < (char*) &fbuf[BSIZE])
		{
			// this skips entries with inode 0, these are to be ignored...
			if (dir->inode == 0)
			{
				cp += dir->rec_len;
				dir = (DIR*) cp;
				continue;
			}
				
			INODE i = inumber_to_inode(dir->inode);
				
			char namebuf[256];
			memcpy(namebuf, dir->name, dir->name_len);
			namebuf[dir->name_len] = 0;
			
			char modebuf[20] = {0};
			mode_to_str(i.i_mode, modebuf);
			
			
			printf("%-5d  %-10s  %-5d  %-5d  %-5d  %-7d  %-12d  %s",
				dir->inode, modebuf, i.i_links_count, i.i_uid, i.i_gid, i.i_size, i.i_mtime, namebuf
				);
				
			if ((i.i_mode & S_IFLNK) == S_IFLNK)
				printf(" -> %s", (char*) &i.i_block[0]);
			
			printf("\n");
			
			cp += dir->rec_len;
			dir = (DIR*) cp;
		}
		
		DEBUG("Looking in next block now\n");
	}
	
	return 1;	
}
