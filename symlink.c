#include "ext2_sim.h"

int ext2_symlink(int argc, char* argv[])
{
	int result;
	INODE inode;
	int inum;
	
	if (argc < 3)
	{
		printf("Must specify the link target and link name\n");
		return 0;
	}
	
	result = path_to_inode(argv[1], &inode, &inum, FOLLOW_SYMLINKS);
		
	if (!result)
	{
		printf("Link target does not exist\n");
		return 0;
	}
	
	if (strlen(argv[1]) > 59)
	{
		printf("Link target must be less than 60 characters\n");
		return 0;
	}
	
	char* creat_argv[] = {"creat", argv[2]};
	
	result = ext2_creat(2, creat_argv);
	
	if (!result)
	{
		printf("There was a problem creating the link file\n");
		return 0;
	}
	
	INODE link_inode;
	int link_inum;
	result = path_to_inode(argv[2], &link_inode, &link_inum, FOLLOW_SYMLINKS_EXCEPT_LAST);
	
	if (!result)
	{
		printf("COuld not read the link file we just created, big problem!\n");
		return 0;
	}
	
	int old_mode = link_inode.i_mode;
	DEBUG("old_mode = %x\n", old_mode);
	

	int permissions = old_mode & (S_IRWXU | S_IRWXG | S_IRWXO);
	DEBUG("permissions = %x\n", permissions);
	
	int new_mode = S_IFLNK | permissions;
	DEBUG("new_mode = %x\n", new_mode);

	link_inode.i_mode = new_mode;
	strcat((char*) &link_inode.i_block[0], argv[1]);
	link_inode.i_size = strlen(argv[1]);
	
	DEBUG("Data in the inode: %s\n", (char*) &link_inode.i_block[0]);
	
	write_inode(link_inode, link_inum);
	
	return 1;	
}
