#include "ext2_sim.h"

int ext2_stat(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must supply a path for stat\n");	
		return 0;
	}
	
	struct stat s;
	
	INODE inode;
	int inum;
	int result;
	
	result = path_to_inode(argv[1], &inode, &inum, FOLLOW_SYMLINKS);
	
	if (!result)
	{
		printf("Invalid path `%s'\n", argv[1]);	
		return 0;
	}
	
	s.st_dev = 0; // what is this??
	s.st_ino = inum;
	s.st_mode = inode.i_mode;
	s.st_nlink = inode.i_links_count;
	s.st_uid = inode.i_uid;
	s.st_gid = inode.i_gid;
	s.st_size = inode.i_size;
	s.st_blksize = 1024;
	s.st_blocks = inode.i_blocks;
	s.st_atime = inode.i_atime;
	s.st_mtime = inode.i_mtime;
	s.st_ctime = inode.i_ctime;
	
	#define SP(a) printf("%12s: %d\n", #a, (int) a)
	SP(s.st_dev);
	SP(s.st_ino);
	SP(s.st_mode);
	SP(s.st_nlink);
	SP(s.st_uid);
	SP(s.st_gid);
	SP(s.st_size);
	SP(s.st_blksize);
	SP(s.st_blocks);
	SP(s.st_atime);
	SP(s.st_mtime);
	SP(s.st_ctime);
	
	return 1;
	
}
