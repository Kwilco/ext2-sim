#include "ext2_sim.h"

int verify_ext2(void)
{
	read_super();
	read_group();
	
	// compare to magic number
	return (S_BLOCK.s_magic == EXT2_MAGIC);
}

int ext2_mount_root(char* device)
{
	DEBUG("Attempting to mount root device\n");
	ROOT_DEVICE = device;
	
	ROOT_FD = open(ROOT_DEVICE, O_RDWR | O_FSYNC);
	
	if (ROOT_FD < 1)
	{
		printf("Could not open %s for rw\n", ROOT_DEVICE);
		return 0;
	}
	else
	{
		DEBUG("Opened device successfully\n");
	}
	
	
	if (!verify_ext2())
	{
		printf("Not a valid ext2 filesystem\n");
		return 0;
	}
	else
	{
		DEBUG("Verified valid ext2 filesystem\n");
	}
	
	if (root_inode().i_block[0] == 0)
	{
		printf("The root inode appears to be empty...\n");
		return 0;
	}

	DEBUG("Successfully mounted %s as root!\n", ROOT_DEVICE);
	
	return 1;
}
