#include "ext2_sim.h"

int ext2_cp(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("Must provide source, destination\n");
		return 0;
	}
	
	int sfd = ext2_open_actual(argv[1], MODE_R);
	
	if (sfd < 0)
	{
		printf("Could not open `%s' for read]\n", argv[1]);
		return 0;
	}
	
	int dfd = ext2_open_actual(argv[2], MODE_W);
	
	if (dfd < 0)
	{
		printf("Could not open `%s' for write\n", argv[2]);
		return 0;
	}
	
	char buf[BSIZE];
	int r;
	int w;
	int total = 0;
	
	
	while ((r = ext2_read_actual(sfd, buf, BSIZE)) > 0)
	{
		DEBUG("read %d, now going to write that much...\n", r);
		w = ext2_write_actual(dfd, buf, r);
		DEBUG("Wrote %d bytes...\n", w);
		total += w;
	}
	
	DEBUG("Wrote %d bytes from %s to %s\n", total, argv[1], argv[2]);
	
	ext2_close_actual(sfd);
	ext2_close_actual(dfd);
	
	INODE si;
	int sin;
	INODE ci;
	int cin;
	
	path_to_inode(argv[1], &si, &sin, FOLLOW_SYMLINKS);
	path_to_inode(argv[2], &ci, &cin, FOLLOW_SYMLINKS);
	
	DEBUG("size of original: %d\n", si.i_size);
	DEBUG("Size of copy: %d\n", ci.i_size);
	DEBUG("size equal = %d\n", (si.i_size == ci.i_size));
	
	return 1;
}
