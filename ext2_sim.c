#include "ext2_sim.h"

char* ROOT_DEVICE;
int ROOT_FD;

SUPER S_BLOCK;
GD G_DESC;
int INODES_BEGIN_BLOCK;

int CWD_INUM = EXT2_ROOT_INO;


// removes trailing \n from str
void chop(char* str)
{
	while (*str != 0)
		str++;
		
	str--;
	
	if (*str == '\n')
		*str = 0;
}

int cmd_is_quit(char* cmd)
{
	return (strcmp(cmd, "quit") == 0);
}

void prompt(char* buf)
{
	char cwd_buf[BSIZE] = {0};
	ext2_pwd_actual(CWD_INUM, cwd_buf);
	printf("%s > ", cwd_buf);
	
	fgets(buf, BSIZE, stdin);
	
	if (strcmp(buf, "\n") != 0)
		printf("\033[35m" "%s" "\033[0m", buf);
	chop(buf);
}

void ext2_sim_run(char* device)
{
	DEBUG("Running sim on `%s'\n", device);
	
	if (!ext2_mount_root(device))
	{
		printf("Mount root failed, aborting simulator\n");
		return;
	}
	
	char command[BSIZE];
	
	do
	{
		prompt(command);
		handle_command(command);
	} while (!cmd_is_quit(command));
	
	DEBUG("Simulator quitting\n");
}
