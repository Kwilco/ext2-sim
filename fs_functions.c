#include "ext2_sim.h"

// returns the inode, given an inumber
INODE inumber_to_inode(int inum)
{
	char fbuf[BSIZE];
	
	int block = ((inum - 1) / 8) + INODES_BEGIN_BLOCK;
	int offset = ((inum - 1) % 8);
		
	rblock(fbuf, block);
	
	INODE* iptr = (INODE*) fbuf;
	iptr += offset;
	
	return *iptr;
}

INODE root_inode(void)
{
	return inumber_to_inode(2);
}


// searchs in a directory listing for an item in that directory and returns its inum?
int search(INODE* inode_ptr, char* name)
{
	INODE inode = *inode_ptr;
	char fbuf[BSIZE];
	
	// for each block in the directory
	int i = 0;
	int b;
	for (i = 0; (b = logical_block_to_block(inode, i)) != 0; i++)
	{
		DEBUG("Looking in block %d\n", b);
		rblock(fbuf, b);
	
		DIR* dir = (DIR*) fbuf;
		char* cp = (char*) dir;
		
		while ((char*) dir < (char*) &fbuf[BSIZE])
		{
			char namebuf[256];
			strncpy(namebuf, dir->name, dir->name_len);
			namebuf[dir->name_len] = 0;
			//printf("%4d %4d %4d %s\n", dir->inode, dir->rec_len, dir->name_len, namebuf);
			
			// compare against name
			if (strcmp(name, namebuf) == 0)
			{
				//printf("Found %s, inumber=%d\n", name, dir->inode);
				return dir->inode;
			}
			
			cp += dir->rec_len;
			dir = (DIR*) cp;
		}
	}

	return 0;
}




// searches from the current inode for a filepath, loads inum and inode into dest and dest_inum
int find_inode(INODE curr, int curr_inum, char* filepath, INODE* dest, int* dest_inum, int symlink)
{
	DEBUG("Calling find_inode on inum=%d, with filepath=%s, symlink=%d\n", curr_inum, filepath, symlink);
	char filenamebuf[1024];
	char* components[256];
	int n = 0;
	strcpy(filenamebuf, filepath);
	
	char* temp = strtok(filenamebuf, "/");
	
	while (temp != NULL)
	{
		components[n] = temp;
		n++;
		temp = strtok(NULL, "/");
	}
	
	int i;
	int old_inum;
	INODE old_curr;
	int inum = curr_inum; // start off at current inum
	for (i = 0; i < n; i++)
	{
		old_inum = inum;
		old_curr = curr;
		// make sure curr is a dir
		if (!S_ISDIR(curr.i_mode))
		{
			DEBUG("Path component not a directory\n");
			return 0;
		}
		
		inum = search(&curr, components[i]);
		if (!inum)
		{
			DEBUG("Could not find %s\n", components[i]);
			return 0;
		}
		else
		{
			DEBUG("Found %s and moved curr to inum=%d\n", components[i], inum);
		}
		
		curr_inum = inum;
		curr = inumber_to_inode(inum);
		

		// see if we are dealing with a symlink, and if so, resolve surrent to whatever it points to
		if ((curr.i_mode & S_IFLNK) == S_IFLNK)
		{
			DEBUG("We are dealing with a symlink here, how to deal with it...");
			if (symlink == NO_FOLLOW_SYMLINK)
			{
				DEBUG(" Don't follow at all\n");
				continue;
			}

			if (symlink == FOLLOW_SYMLINKS_EXCEPT_LAST && (i == n - 1))
			{
				DEBUG("Not following the last symlink `%s'\n", components[i]);
				continue;
			}
			
			char* symlink_path = (char*) &curr.i_block[0];
			DEBUG("Search has encountered a symlink, following it to %s\n", symlink_path);
			INODE curr_copy = old_curr;
			int symlink_result;
			symlink_result = find_inode(curr_copy, old_inum, symlink_path, &curr, &inum, symlink);
			
			if (!symlink_result)
			{
				DEBUG("Failed to follow symlink, bailing\n");
				return 0;
			}
			
			DEBUG("Returning from our symlink adventure... hopefully you wanted one of those...\n");
		}

		
	}
	
	*dest = curr;
	*dest_inum = inum;
	return 1;
}


void read_super(void)
{
	char fbuf[BSIZE];
	
	// load block
	rblock(fbuf, 1);
	
	SUPER* g = (SUPER*) fbuf;

	// load into global
	S_BLOCK = *g;
}

void read_group(void)
{
	char fbuf[BSIZE];
	
	// load block
	rblock(fbuf, 2);
	
	GD* g = (GD*) fbuf;

	// load into global
	G_DESC = *g;
	INODES_BEGIN_BLOCK = G_DESC.bg_inode_table;
}

void write_super(void)
{
	char fbuf[BSIZE];
	
	memcpy(fbuf, &S_BLOCK, sizeof(S_BLOCK));
	wblock(fbuf, 1);
}

void write_group(void)
{
	char fbuf[BSIZE];
	
	memcpy(fbuf, &G_DESC, sizeof(G_DESC));
	wblock(fbuf, 2);
}

int test_bit(char* buf, int i)
{
	int byte = (i / 8);
	int offset = (i % 8);
	
	char mask = 0x01;
	
	mask <<= offset;
	
	//printf("Testing in byte %d at offset %d with mask %d\n", byte, offset, mask);
	
	return mask & buf[byte];
}


int set_bit(char* buf, int i)
{
	int byte = (i / 8);
	int offset = (i % 8);
	
	char mask = 0x01;
	
	mask <<= offset;
	
	buf[byte] |= mask;
	
	return 1;
}

int clear_bit(char* buf, int i)
{
	int byte = (i / 8);
	int offset = (i % 8);
	
	unsigned char mask = 0x01;
	
	mask <<= offset;
	
	mask = ~mask;
	
	buf[byte] &= mask;
	
	return 1;
}

int inode_alloc(void)
{
	char buf[BSIZE];
	read_group();
	read_super();
	
	int ibmp = G_DESC.bg_inode_bitmap;
	//DEBUG("Inode bitmap at block %d\n", ibmp);
	
	rblock(buf, ibmp);
	
	int ninodes = 184; // WARNING, MAGIC NUMBER. NEED TO FIGURE OUT WHY IT IS 184
	int i;
	for (i = 0; i < ninodes; i++)
	{
		if (test_bit(buf, i) == 0)
		{
			//printf("Bit number %d is a zero!\n", i);
			set_bit(buf, i);
			
			S_BLOCK.s_free_inodes_count -= 1;
			G_DESC.bg_free_inodes_count -= 1;
			
			// write ibmp block
			wblock(buf, ibmp);
			
			// write super block
			write_super();
			
			// write gd
			write_group();
			
			return i + 1; // bit -> inumber
		}
	}
	
	printf("No free iblocks were found in the bitmap.\n");
	return 0;
}


int inode_dealloc(int inum)
{
	char buf[BSIZE];
	read_group();
	read_super();

	int ibmp = G_DESC.bg_inode_bitmap;
	//DEBUG("Inode bitmap at block %d\n", ibmp);
	
	rblock(buf, ibmp);
	
	int bit = inum - 1;
	
	if (test_bit(buf, bit) == 0)
	{
		DEBUG("WARNING, attempting to deallocate unallocated inode %d\n", inum);
		return 0;
	}
	
	clear_bit(buf, bit);

	S_BLOCK.s_free_inodes_count += 1;
	G_DESC.bg_free_inodes_count += 1;
	
	// write ibmp block
	wblock(buf, ibmp);
	
	// write super block
	write_super();
	
	// write gd
	write_group();
				
	return 1;
}



int write_inode(INODE i, int inum)
{
	char fbuf[BSIZE];
	
	int block = ((inum - 1) / 8) + INODES_BEGIN_BLOCK;
	int offset = ((inum - 1) % 8);
	
	rblock(fbuf, block);
	
	INODE* iptr = (INODE*) fbuf;
	iptr += offset;
	
	*iptr = i;
	
	wblock(fbuf, block);
	
	return 1;
}



int block_alloc(void)
{
	char buf[BSIZE];
	read_group();
	read_super();
	
	int bbmp = G_DESC.bg_block_bitmap;
	//DEBUG("Block bitmap at block %d\n", bbmp);
	
	rblock(buf, bbmp);
	
	int nblks = 1440; // WARNING, MAGIC NUMBER.
	int i;
	for (i = 0; i < nblks; i++)
	{
		if (test_bit(buf, i) == 0)
		{
			//printf("Bit number %d is a zero!\n", i);
			set_bit(buf, i);
			
			S_BLOCK.s_free_blocks_count -= 1;
			G_DESC.bg_free_blocks_count -= 1;
			
			// write ibmp block
			wblock(buf, bbmp);
			
			// write super block
			write_super();
			
			// write gd
			write_group();
			
			char bbuf[BSIZE];
			rblock(bbuf, i + 1);
			bzero(bbuf, BSIZE);
			wblock(bbuf, i + 1);
			
			return i + 1; // bitmap to block num
		}
	}
	
	printf("No free data blocks were found in the bitmap.\n");
	return 0;
}

int block_dealloc(int bnum)
{
	char buf[BSIZE];
	read_group();
	read_super();
	
	int bbmp = G_DESC.bg_block_bitmap;
	//DEBUG("Block bitmap at block %d\n", bbmp);
	
	rblock(buf, bbmp);
	
	int bit = bnum - 1;
	
	if (test_bit(buf, bit) == 0)
	{
		DEBUG("WARNING, attempting to deallocate unallocated block %d\n", bnum);
		return 0;
	}
	
	clear_bit(buf, bit);
	
	S_BLOCK.s_free_blocks_count += 1;
	G_DESC.bg_free_blocks_count += 1;
	
	// write ibmp block
	wblock(buf, bbmp);
	
	// write super block
	write_super();
	
	// write gd
	write_group();
	
	//DEBUG("B%d ", bnum);
	
	return 1;
}

// resolves a path to inode+inum, supports absolute and relative paths
int path_to_inode(char* path, INODE* inode, int* inum, int symlink)
{
	if (path[0] == '/')
		return find_inode(root_inode(), EXT2_ROOT_INO, path, inode, inum, symlink);
	else
		return find_inode(inumber_to_inode(CWD_INUM), CWD_INUM, path, inode, inum, symlink);
}


// searchs in a directory listing for an item in that directory and returns its inum?
int search_dir_inum_to_path(INODE* inode_ptr, int inum, char* buf)
{
	INODE inode = *inode_ptr;
	char fbuf[BSIZE];
	
	// for each block in the directory
	int i = 0;
	int b;
	for (i = 0; (b = logical_block_to_block(inode, i)) != 0; i++)
	{

		// for each dir in the block, look for name
		rblock(fbuf, b);
	
		DIR* dir = (DIR*) fbuf;
		char* cp = (char*) dir;
		
		while ((char*) dir < (char*) &fbuf[BSIZE])
		{
			char namebuf[256];
			strncpy(namebuf, dir->name, dir->name_len);
			namebuf[dir->name_len] = 0;
			//printf("%4d %4d %4d %s\n", dir->inode, dir->rec_len, dir->name_len, namebuf);
			
			// compare against name
			if ((int) dir->inode == inum)
			{
				//printf("Found %s, inumber=%d\n", name, dir->inode);
				strcpy(buf, namebuf);
				return 1;
			}
			
			cp += dir->rec_len;
			dir = (DIR*) cp;
		}
	}

	return 0;
}

// assumes the child is not in the directory. caller must check this
int add_child(INODE parent_inode, int parent_inum, char* child_name, int child_inum)
{	
	int i;
	int b;
	DIR* dir;
	char* cp;
	char pbuf[BSIZE];
	int parent_dir_data_block_num;

	for (i = 0; (b = logical_block_to_block(parent_inode, i)) != 0; i++)
	{
		parent_dir_data_block_num = b;
		DEBUG("Looking in block %d\n", parent_dir_data_block_num);
		
		rblock(pbuf, parent_dir_data_block_num);
		
		dir = (DIR*) pbuf;
		cp = (char*) dir;
		
		while ((char*) dir < (char*) &pbuf[BSIZE])
		{
			DEBUG("rec_len=%d,name_len=%d,inum=%d\n", dir->rec_len, dir->name_len, dir->inode);
			// see if we can fit the new name in here
			if ((min_len(dir) < dir->rec_len) && (ideal_len(child_name) <= (dir->rec_len - min_len(dir))))
			{
				int dir_len = dir->rec_len;
				int d_min_len =  min_len(dir);
				
				DEBUG("found record to add onto, changing length of record from %d to %d\n", dir_len, min_len(dir));
				dir->rec_len = d_min_len;
				
				cp += dir->rec_len;
				dir = (DIR*) cp;
				
				dir->rec_len = dir_len - d_min_len;
				dir->name_len = strlen(child_name);
				dir->inode = child_inum;
				
				DEBUG("new record entered, rec_len=%d, name_len=%d, inode=%d\n", dir->rec_len, dir->name_len, dir->inode);
				
				memcpy(dir->name, child_name, dir->name_len);
				DEBUG("added to record, done!\n");
				
				wblock(pbuf, parent_dir_data_block_num);

				return 1;
			}
			
			cp += dir->rec_len;
			dir = (DIR*) cp;
		}
		
		printf("Reached the end of block %d, looking in next block\n", b);
	}
	
	
	b = allocate_logical_block(parent_inode, parent_inum, i);
	parent_inode = inumber_to_inode(parent_inum);

	
	parent_inode.i_size += 1024;
	write_inode(parent_inode, parent_inum);
	
	if (b == 0)
	{
		printf("Could not allocate the new block as required\n");
		return 0;
	}
	
	DEBUG("Allocated a new block %d\n", b);
	
	parent_dir_data_block_num = b;
	bzero(pbuf, BSIZE);
		
	dir = (DIR*) pbuf;
	cp = (char*) dir;

				
	dir->rec_len = 1024;
	dir->name_len = strlen(child_name);
	dir->inode = child_inum;
	
	DEBUG("new record entered, rec_len=%d, name_len=%d, inode=%d\n", dir->rec_len, dir->name_len, dir->inode);
	
	memcpy(dir->name, child_name, dir->name_len);
	DEBUG("added to record, done!\n");
	
	wblock(pbuf, parent_dir_data_block_num);

	return 1;
}



int remove_child(INODE parent_inode, char* child)
{
	int i;
	int b;
	for (i = 0; (b = logical_block_to_block(parent_inode, i)) != 0; i++)
	{
		// find data block for parent
		int data_block = b;
		
		DEBUG("data_block = %d\n", data_block);
		
		char pbuf[BSIZE];
		rblock(pbuf, data_block);
		
		DIR* dir = (DIR*) pbuf;
		char* cp = (char*) dir;
		int prev_len = 0;
		
		while ((char*) dir < (char*) &pbuf[BSIZE])
		{
			DEBUG("rec_len=%d,name_len=%d,inum=%d\n", dir->rec_len, dir->name_len, dir->inode);
			// see if we can fit the new name in here
			
			char namebuf[256];
			memcpy(namebuf, dir->name, dir->name_len);
			namebuf[dir->name_len] = 0;
			
			if (strcmp(namebuf, child) == 0)
			{
				// envelop the old entry
				DIR* expanding_dir = (DIR*) (cp - prev_len);
				expanding_dir->rec_len += dir->rec_len;
				
				DEBUG("expanded over top of the old entry, it's gone now\n");
				wblock(pbuf, data_block);
				return 1;
			}
			
			prev_len = dir->rec_len;
			cp += dir->rec_len;
			dir = (DIR*) cp;
		}
		DEBUG("Searching next block\n");
	}

	printf("Child not found in the directory its trying to be removed from\n");
	return 0;
}

int indirect(int indirect_block, int offset)
{
	if (offset >= 256 || offset < 0)
	{
		printf("Offset of %d is over 255 or negative, both of which are bogus for indirect blocks\n", offset);
		return 0;
	}
	
	char buf[BSIZE];
	int* ibuf = (int*) buf;
	rblock(buf, indirect_block);
	
	return ibuf[offset];
}


int dindirect(int dindirect_block, int offset)
{
	if (offset >= 256*256 || offset < 0)
	{
		printf("Offset of %d is over 256*256-1 or negative, both of which are bogus for double indirect blocks\n", offset);
		return 0;
	}
	
	char buf[BSIZE];
	int* ibuf = (int*) buf;
	rblock(buf, dindirect_block);
	
	int indirect_block = ibuf[offset / 256];
	int indirect_offset = offset % 256;
	
	return indirect(indirect_block, indirect_offset);
}


// converts an absolute offset into the appropriate block number.
//assumes the blocks exist in the file
int logical_block_to_block(INODE inode, int lblk)
{
	if (lblk < 0)
	{
		printf("Why are you asking for a negative BLOCK NUMBER?? AHHHHHHHHHHH!!!!!\n");
		return 0;
	}
		
	if (lblk < EXT2_NDIR_BLOCKS)
	{
		//DEBUG("THis is a direct block...");
		return inode.i_block[lblk];
	}
		
	if (lblk < 256 + EXT2_NDIR_BLOCKS)
	{
		//DEBUG("This is an indirect block...");
		return indirect(inode.i_block[EXT2_IND_BLOCK], lblk - EXT2_NDIR_BLOCKS);
	}
		
	// assume no triple-indirect blocks	
	//DEBUG("THis is a double indirect block...");
	return dindirect(inode.i_block[EXT2_DIND_BLOCK], lblk - EXT2_NDIR_BLOCKS - 256);
}

int truncate_file(INODE inode, int inum)
{	
	read_group();
	int free_blocks = G_DESC.bg_free_blocks_count;
	
	//DEBUG("Deallocating the following blocks: ");
	int i = 0;
	int b;
	for (i = 0; (b = logical_block_to_block(inode, i)) != 0; i++)
	{
		block_dealloc(b);
	}
	
	if (inode.i_block[EXT2_IND_BLOCK] != 0)
	{
		block_dealloc(inode.i_block[EXT2_IND_BLOCK]);
	}
	
	if (inode.i_block[EXT2_DIND_BLOCK] != 0)
	{
		char buf[BSIZE];
		int* ibuf = (int*) buf;
		rblock(buf, inode.i_block[EXT2_DIND_BLOCK]);
		
		for (i = 0; i < 256; i++)
		{
			if (ibuf[i] != 0)
			{
				block_dealloc(ibuf[i]);
			}
		}
		
		
		block_dealloc(inode.i_block[EXT2_DIND_BLOCK]);
	}
	
	for (i = 0; i <= EXT2_DIND_BLOCK; i++)
		inode.i_block[i] = 0;
		
	inode.i_atime = time(NULL);
	inode.i_mtime = time(NULL);
	inode.i_size = 0;

	write_inode(inode, inum);

	read_group();
	DEBUG("File truncated, freed %d blocks\n", G_DESC.bg_free_blocks_count - free_blocks);

	return 1;
}

int empty_directory_block(int block)
{
	char fbuf[BSIZE];
	rblock(fbuf, block);
	
	DIR* dir = (DIR*) fbuf;
	char* cp = (char*) dir;
		
	while ((char*) dir < (char*) &fbuf[BSIZE])
	{
		// this skips entries with inode 0, these are to be ignored...
		if (dir->inode == 0)
		{	
			cp += dir->rec_len;
			dir = (DIR*) cp;
			continue;
		}
						
		char namebuf[256];
		memcpy(namebuf, dir->name, dir->name_len);
		namebuf[dir->name_len] = 0;
		
		// if its not . or .. there's something here
		if ((strcmp(namebuf, ".") != 0) && (strcmp(namebuf, "..") != 0))
			return 0;
		
		cp += dir->rec_len;
		dir = (DIR*) cp;
	}

	return 1;
}


int directory_is_empty(INODE inode)
{
	int i = 0;
	int b;
	for (i = 0; (b = logical_block_to_block(inode, i)) != 0; i++)
	{
		if (!empty_directory_block(b))
			return 0;
	}
	
	return 1;
}

int directory_is_busy(int inum)
{
	int current_inum = CWD_INUM;
	INODE current_inode = inumber_to_inode(current_inum);
	
	// do this for all procs in lvl3
	do {
		if (inum == CWD_INUM)
			return 1;
		
		INODE parent_inode;
		int parent_inum;
		int result = find_inode(current_inode, current_inum, "..", &parent_inode, &parent_inum, NO_FOLLOW_SYMLINK);
		
		if (!result)
		{
			printf("Something wrong, can't find parent\n");
			return 1;
		}
		
		current_inode = parent_inode;
		current_inum = parent_inum;
	} 	while (current_inum != 2); //MAGIC NUMBER
	
	return 0;
}
