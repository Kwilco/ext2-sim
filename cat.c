#include "ext2_sim.h"

int ext2_cat(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Need to specify file to cat\n");
		return 0;
	}
	
	char buf[BSIZE] = {0};
	int fd = ext2_open_actual(argv[1], MODE_R);
	
	if (fd < 0)
	{
		printf("Could not open `%s'. It may be open for writing...\n", argv[1]);
		return 0;
	}
	
	int r;
	
	while ((r = ext2_read_actual(fd, buf, BSIZE)) != 0)
	{
		int i;
		for (i = 0; i < r; i++)
		{
			printf("%c", buf[i]);
		}
		
		bzero(buf, BSIZE);
	}
	
	ext2_close_actual(fd);
	
	return 1;
}
