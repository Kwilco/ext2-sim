#include "ext2_sim.h"

int min_len(DIR* d)
{
	return 8 + (d->name_len + 3)/4*4;
}

int ideal_len(char* str)
{
	return 8 + (strlen(str) + 3)/4*4;
}

int ext2_mkdir_actual(INODE parent_inode, int parent_inum, char* child)
{
	DEBUG("parent inum = %d\n", parent_inum);
	
	read_super();
	read_group();
	
	if (G_DESC.bg_free_inodes_count < 1)
	{
		printf("No free inodes.\n");
		return 0;
	}
	
	if (G_DESC.bg_free_blocks_count < 2)
	{
		printf("Need at least 2 free data blocks\n");
		return 0;
	}
	
	int inum = inode_alloc();
	
	if (!inum)
	{
		printf("No inum was returned\n");
		return 0;
	}
	else
	{
		DEBUG("Time to make an inode at inum %d!\n", inum);
	}
	
	INODE i;
	
	i.i_mode = S_IFDIR | S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;
	i.i_uid = 0;
	i.i_size = 1024;
	i.i_atime = time(NULL);
	i.i_ctime = time(NULL);
	i.i_mtime = time(NULL);
	i.i_dtime = 0;
	i.i_gid = 0;
	i.i_links_count = 2;
	i.i_blocks = 2; // 2 512-byte blocks
	i.i_flags = 0;
	i.i_dir_acl = 0; // ARRRGGGGGGGGGGHHHHHHHHHHH crazy linux
	
	int j;
	for (j = 0; j <= EXT2_DIND_BLOCK; j++)
		i.i_block[j] = 0;

	// allocate data block for dir listing
	int bnum = block_alloc();
	DEBUG("Alocated block %d\n", bnum);
	i.i_block[0] = bnum;
	i.i_block[1] = 0;
	
	// fill it
	// TODO use DIR struct here, this is hacky
	char bbuf[BSIZE];
	
	int* rec_inum = (int*) bbuf;
	short* rec_len = (short*) (rec_inum + 1);
	short* name_len = rec_len + 1;
	char* name = (char*) (name_len + 1);
	
	*rec_inum = inum;
	*rec_len = 8 + 4;
	*name_len = 1;
	*name = '.';
	
	rec_inum = (int*) &bbuf[12];
	rec_len = (short*) (rec_inum + 1);
	name_len = rec_len + 1;
	name = (char*) (name_len + 1);
	
	*rec_inum = parent_inum;
	*rec_len = 1012;
	*name_len = 2;
	*name = '.';
	*(name + 1) = '.';
	
	// write it
	wblock(bbuf, bnum);	
	write_inode(i, inum);
	
	int result = add_child(parent_inode, parent_inum, child, inum);
	
	if (!result)
	{
		DEBUG("Could not add child\n");
		return result;
	}
			
	// increment parent's link count
	parent_inode = inumber_to_inode(parent_inum);
	parent_inode.i_links_count += 1;
	write_inode(parent_inode, parent_inum);

	return 1;
}



int ext2_mkdir(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must supply a directory to create\n");
		return 0;
	}
	
	char* dir = argv[1];
	char dcopy1[BSIZE];
	char dcopy2[BSIZE];
	
	strcpy(dcopy1, dir);
	strcpy(dcopy2, dir);
	
	DEBUG("dir=%s\n", dir);
	
	char* parent = dirname(dcopy1);
	char* child = basename(dcopy2);
	
	DEBUG("parent=%s, child=%s\n", parent, child);
	
	INODE parent_inode;
	int parent_inum;
	int found = 0;
	
	found = path_to_inode(parent, &parent_inode, &parent_inum, FOLLOW_SYMLINKS);
	
	if (!found) 
	{
		printf("Parent directory %s does not exist\n", parent);
		return 0;
	}
	else
	{
		DEBUG("Parent directory %s exists! inum=%d\n", parent, parent_inum);
	}
	
	INODE child_inode;
	int child_inum;
	found = find_inode(parent_inode, parent_inum, child, &child_inode, &child_inum, FOLLOW_SYMLINKS);
	
	if (found)
	{
		printf("Child %s already exists in directory %s\n", child, parent);
		return 0;
	}
	else
	{
		DEBUG("Child doesn't already exist!\n");
	}
	
	return ext2_mkdir_actual(parent_inode, parent_inum, child);
}
