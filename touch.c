#include "ext2_sim.h"

int ext2_touch(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must supply a path to touch\n");	
		return 0;
	}
	
	INODE inode;
	int inum;
	int result;
	
	result = path_to_inode(argv[1], &inode, &inum, FOLLOW_SYMLINKS);
	
	if (!result)
	{
		DEBUG("Invalid path `%s', attempting to create file\n", argv[1]);
		result = ext2_creat(argc, argv);
		if (result)
		{
			DEBUG("Created file\n");
			return 1;
		}
		else
		{
			printf("Could not create file\n");
			return 0;
		}
	}
	
	inode.i_atime = time(NULL);
	inode.i_mtime = time(NULL);
	
	write_inode(inode, inum);
	
	DEBUG("File atime and mtime updated\n");
	
	return 1;
}
