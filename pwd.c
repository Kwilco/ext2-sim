#include "ext2_sim.h"

void ext2_pwd_actual(int inum, char* buf)
{
	if (inum == EXT2_ROOT_INO)
	{
		strcat(buf, "/");
		return;
	}
	
	int result;
	INODE inode;
	int i;
	result = find_inode(inumber_to_inode(inum), inum, "..", &inode, &i, NO_FOLLOW_SYMLINK);
	
	ext2_pwd_actual(i, buf);
	
	if (!result)
	{
		printf("Something is horribly wrong, this directory does not have a .. entry\n");	
		return;
	}
	
	char namebuf[BSIZE];
	result = search_dir_inum_to_path(&inode, inum, namebuf);
	
	if (!result)
	{
		printf("Something is horribly wrong, parent does not seem to contain its child\n");	
		return;
	}
	
	strcat(buf, namebuf);
	strcat(buf, "/");
}

int ext2_pwd(int argc, char* argv[])
{
	
	char buf[BSIZE] = {0};
	
	ext2_pwd_actual(CWD_INUM, buf);
	
	printf("%s\n", buf);
	
	return 1;
}
