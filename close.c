#include "ext2_sim.h"

int ext2_close_actual(int num)
{
	if (num < 0 || num > BSIZE)
	{
		printf("fd out of range\n");
		return -1;
	}
	
	FD* fd_table = get_fd_table();
	
	if (fd_table[num].inum == 0)
	{
		printf("This fd is not open\n");
		return -1;
	}
	
	int inum = fd_table[num].inum;
	INODE i = inumber_to_inode(inum);
	
	fd_table[num].inum = 0;
	
	DEBUG("%s closed\n", fd_table[num].file_name);
	
	if (i.i_links_count < 1)
	{
		if (!file_is_busy(inum))
		{
			DEBUG("We were the last ones to close a deleted file, time to get rid of it\n");
			DEBUG("Truncating file...\n");
			truncate_file(i, inum);
			DEBUG("Deallocating inode...\n");
			inode_dealloc(inum);
		}
	}
	
	return num;
}


// returns -1 on error
int ext2_close(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("Must specify a fd to close\n");
		return -1;
	}
	
	int fd = atoi(argv[1]);
	
	return ext2_close_actual(fd);
}
