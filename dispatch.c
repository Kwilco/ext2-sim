#include "ext2_sim.h"

int ext2_quit(int argc, char* argv[])
{
	return 1;
}

int find_command(char* command)
{
	char* command_str_table[] = {
	"mkdir", "rmdir", "ls", "cd", "pwd",
	"creat", "link", "unlink", "rm", "symlink",
	"stat", "chmod", "touch", "quit", 
	"open", "close", "read", "write",
	"pfd", "lseek",
	"cat", "cp", "mv",
	"menu",
	NULL };
	
	int i;
	for (i = 0; command_str_table[i] != NULL; i++)
		if (strcmp(command_str_table[i], command) == 0)
			return i;

	return -1;
}



typedef int (*ccfunction)(int, char**);
ccfunction funcTable[] = {
	&ext2_mkdir, &ext2_rmdir, &ext2_ls, &ext2_cd, &ext2_pwd, 
	&ext2_creat, &ext2_link, &ext2_unlink, &ext2_unlink, &ext2_symlink, 
	&ext2_stat, &ext2_chmod, &ext2_touch, &ext2_quit, 
	&ext2_open, &ext2_close, &ext2_read, &ext2_write, 
	&ext2_pfd, &ext2_lseek, 
	&ext2_cat, &ext2_cp, &ext2_mv, 
	&ext2_menu,
	 };

int dispatch(int argc, char* argv[])
{
	int index = find_command(argv[0]);
	
	if (index < 0)
	{
		printf("Invalid command \"%s\"\n", argv[0]);
		return 0;
	}	

	return (*funcTable[index])(argc, argv);
}

int handle_command(char* cmd)
{
	char cmd_copy[BSIZE];
	strcpy(cmd_copy, cmd);
	
	int argc = 0;
	char* argv[BSIZE];
	
	// tokenize here
	argv[argc++] = strtok(cmd_copy, " ");
	
	if (argv[0] == NULL)
		return 0;
	
	while ((argv[argc] = strtok(NULL, " ")))
		argc++;
	
	// pass to appropriate function
	return dispatch(argc, argv);
}
