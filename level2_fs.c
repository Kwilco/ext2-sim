#include "ext2_sim.h"

FD fd_table[BSIZE] = {{0,0,0,{0}}};

FD* get_fd_table(void)
{
	return fd_table;
}

int put_in_fd_table(FD fd)
{
	int i;
	for (i = 0; i < BSIZE; i++)
	{
		if (fd_table[i].inum == 0)
		{
			// found a free spot
			fd_table[i] = fd;
			return i;
		}
	}
	
	return -1;
}

int file_is_busy(int inum)
{
	int i;
	for (i = 0; i < BSIZE; i++)
	{
		if (fd_table[i].inum == inum)
		{
			return 1;
		}
	}
	
	return 0;
}

int str_to_file_open_mode(char* mode_str)
{
	if ((strcmp(mode_str, "r") == 0) || (strcmp(mode_str, "R") == 0))
		return MODE_R;
		
	if ((strcmp(mode_str, "w") == 0) || (strcmp(mode_str, "W") == 0))
		return MODE_W;
		
	if ((strcmp(mode_str, "rw") == 0) || (strcmp(mode_str, "RW") == 0))
		return MODE_RW;
		
	if ((strcmp(mode_str, "a") == 0) || (strcmp(mode_str, "A") == 0))
		return MODE_A;
	
	printf("Unknown mode, assuming you meant read\n");
	return MODE_R;
}

char* file_open_mode_to_str(int mode)
{
	switch (mode)
	{
		case MODE_R:
			return "read";
		case MODE_W:
			return "write";
		case MODE_RW:
			return "read/write";
		case MODE_A:
			return "append";
	}
	
	return "unknown";
}

// searches all open FDs, returns MODE_W if the file is open for write (includes a and rw)
// returns MODE_R if open for read only,
// returns 0 if not open
int get_file_mode(int inum)
{
	int mode = 0;
	int i;
	for (i = 0; i < BSIZE; i++)
	{
		if (fd_table[i].inum == inum)
		{
			if (fd_table[i].mode == MODE_R)
				mode = MODE_R;
			else
				return MODE_W;
		}
	}
	
	return mode;
}


int allocate_logical_block(INODE i, int inum, int lblk)
{
	if (lblk < 0)
	{
		printf("wtf, negative block?\n");
		return 0;
	}
	
	if (lblk < EXT2_NDIR_BLOCKS)
	{
		i.i_block[lblk] = block_alloc();
		i.i_blocks += 2;
		write_inode(i, inum);
		DEBUG("Allocated %d for new direct block\n", i.i_block[lblk]);
		return i.i_block[lblk];
	}
	
	// make an indirect block, cause it needs one!
	if (lblk == EXT2_NDIR_BLOCKS)
	{
		DEBUG("Making new indirect block\n");
		int ind_blk = block_alloc();
		
		if (!ind_blk)
		{
			printf("Could not allocate a new block for use as the indirect block\n");
			return 0;
		}
		
		i = inumber_to_inode(inum);
		i.i_block[EXT2_IND_BLOCK] = ind_blk;
		i.i_blocks += 2;
		write_inode(i, inum);

	}
	
	if (lblk < 256 + EXT2_NDIR_BLOCKS)
	{
		int iblock_offset = lblk - EXT2_NDIR_BLOCKS;
		char iblkbuf[BSIZE];
		rblock(iblkbuf, i.i_block[EXT2_IND_BLOCK]);
		int* blkptr = (int*) iblkbuf;
		int new_blk = block_alloc();
		
		if (!new_blk)
		{
			printf("Could not allocate a new block for use as an indirect under a dind blk\n");
			return 0;
		}
		
		blkptr[iblock_offset] = new_blk;
		wblock(iblkbuf, i.i_block[EXT2_IND_BLOCK]);
		
		i = inumber_to_inode(inum);
		i.i_blocks += 2;
		write_inode(i, inum);
		DEBUG("Allocated %d for new block in indirect space\n", new_blk);
		return new_blk;
	}
	
	
	// make the double indirect block
	if (lblk == 256 + EXT2_NDIR_BLOCKS)
	{
		DEBUG("Making new d-indirect block\n");
		int dind_blk = block_alloc();
		
		if (!dind_blk)
		{
			printf("Could not allocate a new block for use as the dind block\n");
			return 0;
		}
		
		i = inumber_to_inode(inum);
		i.i_block[EXT2_DIND_BLOCK] = dind_blk;
		i.i_blocks += 2;
		write_inode(i, inum);
		
		DEBUG("DIND block set to %d\n", dind_blk);
	}
		
	
	int dind_offset = lblk - 256 - EXT2_NDIR_BLOCKS;
	
	char dbuf[BSIZE];
	int* dptr = (int*) dbuf;
	rblock(dbuf, i.i_block[EXT2_DIND_BLOCK]);
	
	if (dind_offset % 256 == 0)
	{
		// at the beginning of an indirect block, allocate a new one
		DEBUG("at the beginning of an indirect block (under direct), allocate a new one\n");
		int ind = dind_offset / 256;
		DEBUG("Current number at that loc = %d\n", dptr[ind]);
		
		int new_blk = block_alloc();
		
		if (!new_blk)
		{
			printf("Could not allocate a new block for use as an indirect under a dind blk\n");
			return 0;
		}
		
		dptr[ind] = new_blk;
		
		i = inumber_to_inode(inum);
		i.i_blocks += 2;
		write_inode(i, inum);
		
		DEBUG("Allocated new indirect (under dind) block %d\n", new_blk);
	}
	
	int ind_blk = dind_offset / 256;
	int ind_offset = dind_offset % 256;
	
	char ibuf[BSIZE];
	int* iptr = (int*) ibuf;
	rblock(ibuf, dptr[ind_blk]);
	
	int new_blk = block_alloc();
	if (!new_blk)
	{
		printf("Could not allocate a new block for use as an direct under an indirect under a dind blk\n");
		return 0;
	}
	
	iptr[ind_offset] = new_blk;
	
	i = inumber_to_inode(inum);
	i.i_blocks += 2;
	write_inode(i, inum);

	
	wblock(ibuf, dptr[ind_blk]);
	wblock(dbuf, i.i_block[EXT2_DIND_BLOCK]);

	DEBUG("Allocated new direct (under dind and indir) block %d\n", new_blk);


	return new_blk;
}
