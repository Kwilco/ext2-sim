#include "ext2_sim.h"

int ext2_menu(int argc, char* argv[])
{
	printf(
	"Menu of commands for ext2 simulator\n"
	"In the form of:\n"
	"command argument1 argument2 ... argumentN\n\n"
	"mkdir directory_name\n"
	"rmdir directory_name\n"
	"ls directory_name\n"
	"cd directory_name\n"
	"pwd\n"
	"creat file_name\n"
	"symlink target new_name\n"
	"link target new_name\n"
	"unlink file_name\n"
	"stat target\n"
	"chmod mode target\n"
	"touch target\n"
	"open file_name mode (mode can be: r | w | rw | a)\n"
	"close fd\n"
	"lseek fd offset\n"
	"write fd (enter text on the following lines)\n"
	"cat file_name\n"
	"pfd\n"
	"cp source new_name\n"
	"mv source destination\n\n"
	);

	return 1;
}
